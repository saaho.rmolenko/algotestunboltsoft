package com.company;


import java.util.HashSet;
import java.util.Scanner;

public class Main {
    public static HashSet<Integer> numbers = new HashSet<Integer>();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String [] input = in.nextLine().split(" ");

        int n = Integer.parseInt(input[0]);

        for(int i=1;i<input.length;i++){
            numbers.add(Integer.parseInt(input[i]));
        }

        System.out.println(solve(n, n));
    }

    static int solve(int n, int k) {
        if(numbers.contains(k))
            return 0;
        if(k <= n && k > 0)
            return solve(n, k - 1) + solve(n - k, k);
        else if(k > n)
            return solve(n, n);
        else if(n == 0 && k == 0)
            return 1;

        return 0;
    }
}
